$(document).ready(function() {

    $('button[name=load]').on('click', function() {
        $('#jstree').jstree({
            'core': {
                'data': {
                    'url': '/do_query',
                    'data': {'model': $('#model').val()}
                }
            },
            "plugins": [ "search"],
            'check_callback': true
        });
    });


    $('#jstree').on('changed.jstree', function(e, data) {
        console.log(data.selected[0]);
        $.getJSON('/do_select', {'id': data.selected[0]}, function(result) {
            $('#info').html(result.result);
        });
    });

});
