from flask import (Flask, jsonify, render_template, request, session)
from collections import defaultdict
import appyutils as au
import yaml
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)-8s - %(levelname)-8s - %(pathname)s:%(funcName)s():%(lineno)d - %(message)s')

# file handler
fh = logging.FileHandler('app.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

# console handler
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)

app = Flask(__name__)
app.secret_key = 'pbot1234'


@app.route('/do_select')
def do_select():
    id = request.args['id']
    para = [p for p in session['user_data']['topModel']['parameters'] if id == p['para']][0]
    result = '\n'.join('{}: {}'.format(k, v) for k, v in para.items() for k in ['value', 'defValue', 'domain'])
    return jsonify(result='placeholder_' + id)



@app.route('/do_query')
def doit():
    logger.debug('begin')
    model = request.args['model']
    user_data = {'model': model}
    logger.info('model: %s', model)
    model_file = '{}.yaml'.format(model)
    with open(model_file, 'r') as f:
        data = yaml.load(f)
    user_data.setdefault('data', data)
    topModel = [m for m in data['models'] if m['model'] == data['annot']['topModel']][0]
    user_data.setdefault('topModel', topModel)
    model_view = {
        'text': topModel['model'],
        'id': topModel['model'],
        'children': [], 'icon': 'glyphicon glyphicon-plus'}
    for p in topModel['parameters']:
        model_view['children'].append({
            'text': p['para'],
            'id': p['para'],
            'icon': 'glyphicon glyphicon-plus'})
    session['user_data'] = user_data
    return jsonify(model_view)

@app.route('/')
def index():
    session.pop('user_data', None)
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)
