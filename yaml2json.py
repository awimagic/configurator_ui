import yaml


with open('fintech.yaml', 'r') as f:
    d = yaml.load(f)

standin = []
for model in d['models']:
    x = {'text': model['model'], 'children': []}
    for param in model['parameters']:
        x['children'].append(param['para'])
    standin.append(x)

print(standin)

print('topmodel:', d['annot']['topModel'])
